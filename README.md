# QuantVC回测库 v0.1
## Platform
Python:2.7.*
## Features
* 基于事件的历史数据的回测：`onInit()`、`onBar()`、`onStop()`
* 自定义测试策略：`class myTest(BackTest)`
* 自定义绘制回测中的变量：历史数据、技术指标、动态资金曲线、保证金占用率、开平仓时间统计
* 基本回测报告：收益、盈亏比、年化收益率、最大回撤、收闪风险比
* 持仓周期统计：持仓周期、盈利持仓、亏损持仓
* 开平仓时间统计：开平仓时间统计、盈利开平仓、亏损开平仓
* 生成交易记录：默认命名为`TradingHistory.xlsx`
* 跟据交易记录生成回测报告：
  ```
report=BTReport('data\\TradingHistory.xlsx')
report.periodReport()
report.commissionReport()
  ```
* 基于收益、盈亏比、收益风险比的自定义参数优化：`onOptInit()`
* 排列函数（多参数穷举优化会用到）：`permutations(*lists)`
* 多品种跨市回测：
  ```
prepareArbitrageData(source,dest=None,sheet=None,period=None)
prepareExchangeRate(source,dest,sheets)
  ```
* excel读写：`class rwExcel`
## Independece
* matplotlib v1.4.3
* pandas v0.16.2
* talib v0.4.9
## Specials
解压后将BackTest文件夹拷到工作目录。

