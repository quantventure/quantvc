#-*- coding: utf-8 -*-
#Paperrun函数集
#2014.7.1 Powered by hhhcj

from collections import deque
import datetime
from __init__ import prepareArbitrageData
import MySQLdb
import numpy as np

########################################################################
class Paperrun(object):
    """实时测试类"""

    #----------------------------------------------------------------------
    def __init__(self,sheets,**kwargs):#
        """Constructor
        
        参数：
        host='localhost'
        user='root'
        db='test'
        bandWindow=471
        table='agclose'
        sheet1="AG1412"#,EURUSD
        sheet2="AGT+D"#,USDJPY
        threshold=2
        """
        self.sheets=sheets
        if 'host' not in kwargs:
            kwargs['host']='localhost'
        if 'user' not in kwargs:
            kwargs['user']='root'
        if 'db' not in kwargs:
            kwargs['db']='test'
        if 'bandWindow' not in kwargs:
            kwargs['bandWindow']=471
        if 'table' not in kwargs:
            kwargs['table']='agclose'
        #if 'sheet1' not in kwargs:
            #kwargs['sheet1']='AG1412'
        #if 'sheet2' not in kwargs:
            #kwargs['sheet2']='AGT+D'
        if 'threshold' not in kwargs:
            kwargs['threshold']=2
        if 'history' not in kwargs:
            kwargs['history']='history'
        if 'maxDisplay' not in kwargs:
            kwargs['maxDisplay']=256
        self.host=kwargs['host']
        self.user=kwargs['user']
        self.db=kwargs['db']
        self.bandWindow=kwargs['bandWindow']
        self.table=kwargs['table']
        #self.sheet1=kwargs['sheet1']
        #self.sheet2=kwargs['sheet2']
        self.entryZscore=kwargs['threshold']
        self.history_tb=kwargs['history']
        self.maxDisplay=kwargs['maxDisplay']
        
        self.openConnection()
        multiplier=1
        self.dataQueue={
            #self.sheet1:{'datetime':[0]*self.bandWindow,'price':[0]*self.bandWindow,'ask':[0]*self.bandWindow,'bid':[0]*self.bandWindow},
            #self.sheet2:{'datetime':[0]*self.bandWindow,'price':[0]*self.bandWindow,'ask':[0]*self.bandWindow,'bid':[0]*self.bandWindow},
           'datetime':[0]*multiplier*self.bandWindow,
           'diff':[np.nan]*multiplier*self.bandWindow,
           'std':[np.nan]*multiplier*self.bandWindow,
           'zscore':[np.nan]*multiplier*self.bandWindow
        }
        self.marketPosition={}
        self.commission={}
        self.transactions={}
        for symbol in sheets:
            self.dataQueue[symbol]={
                'datetime':[0]*self.bandWindow*multiplier,
                'price':[0]*self.bandWindow*multiplier,
                'ask':[0]*self.bandWindow*multiplier,
                'bid':[0]*self.bandWindow*multiplier
            }
            self.marketPosition[symbol]=0
            self.commission[symbol]=.75*.0001
            self.transactions[symbol]={"OpenTime":[],'Type':[],'Size':[],'OpenPrice':[],'OpenCommission':[],
                                      'CloseTime':[],'ClosePrice':[],'CloseCommission':[],'Profit':[]}
        self.dataDiff={
            'datetime':[datetime.datetime.now()]*self.bandWindow*multiplier,
            'diff':[np.nan]*self.bandWindow*multiplier
        }
        self.stoploss=0
        self.tradingCount=0
    
    #----------------------------------------------------------------------
    def __delete__(self):
        """在对象销亡时执行(即彻底不再使用时),但执行时间不能确定"""
        self.conn.close()
        
    #----------------------------------------------------------------------
    def openConnection(self):
        """"""
        self.conn=MySQLdb.connect(host=self.host,user=self.user,db=self.db)#,charset='uft8'
        self.cursor=self.conn.cursor()
        
    #----------------------------------------------------------------------
    def importData(self,source='data\\agtemp.xlsx',period=(datetime.datetime(2014,5,22,8),datetime.datetime(2014,5,26,8))):
        """写入最新数据
        """
        sheets=prepareArbitrageData(source,None,('fu', 'td'),period=period)#'data\\temp2.csv'
        #写入前检查时间列是否已存在
        sql="select * from "+self.table+" where date>='"+str(sheets.index[0])+"'"
        self.cursor.execute(sql)
        for row in self.cursor.fetchall():
            print "Records have been added twice!!"
            raise
        for i in sheets.index:
            n=self.cursor.execute("insert into "+self.table+"(fuclose,tdclose,date) values(%s,%s,%s)",
                             [float(sheets['fu'][i]),float(sheets['td'][i]),i])
        self.conn.commit()
        print "Successfully write to",self.table
        print "Number of rows affected: ",len(sheets.index)
        
        
    #----------------------------------------------------------------------
    def readData(self):
        """读取最新BandWindow个数据"""
        self.cursor.execute("select * from "+self.table+" order by id desc limit %s",self.bandWindow)
        i=len(self.dataQueue['diff'])
        sheet1=self.sheets[0]
        sheet2=self.sheets[1]
        for row in self.cursor.fetchall():
            #row:id、fuClose、tdClose、date
            i-=1
            #tuple indices must be integers, not str
            self.dataQueue[sheet1]['price'][i]=row[1]
            self.dataQueue[sheet1]['ask'][i]=row[1]
            self.dataQueue[sheet1]['bid'][i]=row[1]
            self.dataQueue[sheet1]['datetime'][i]=row[3]#datetime.datetime.strptime(row[3],'%Y-%m-%d %H:%M:%S')
            self.dataQueue[sheet2]['price'][i]=row[2]
            self.dataQueue[sheet2]['ask'][i]=row[2]
            self.dataQueue[sheet2]['bid'][i]=row[2]
            self.dataQueue[sheet2]['datetime'][i]=row[3]#datetime.datetime.strptime(row[3],'%Y-%m-%d %H:%M:%S')
            self.dataQueue['datetime'][i]=row[3]#datetime.datetime.strptime(row[3],'%Y-%m-%d %H:%M:%S')
            self.dataQueue['diff'][i]=row[1]-row[2]
            self.dataDiff['datetime'][i]=row[3]#datetime.datetime.strptime(row[3],'%Y-%m-%d %H:%M:%S')
            self.dataDiff['diff'][i]=row[1]-row[2]
        self.dataQueue['zscore'][-1]=(self.dataDiff['diff'][-1]-np.mean(self.dataDiff['diff'][-self.bandWindow:]))/ \
            np.std(self.dataDiff['diff'][-self.bandWindow:])
        self.dataQueue['std'][-1]=np.std(self.dataDiff['diff'][-self.bandWindow:])
        
    #----------------------------------------------------------------------
    def readPosition(self):
        """读取持仓信息"""
        for symbol in self.sheets:
            self.cursor.execute("select * from history where symbol='"+symbol+"' order by id desc limit 1")
            for row in self.cursor.fetchall():
                if row[7]==0:
                    if row[1]=='Long':
                        self.marketPosition[symbol]=row[9]
                    elif row[1]=='Short':
                        self.marketPosition[symbol]=-row[9]
                    self.transactions[symbol]["Type"].append(row[1])
                    self.transactions[symbol]["OpenTime"].append(row[3])
                    self.transactions[symbol]["OpenPrice"].append(row[4])
                    self.transactions[symbol]["OpenCommission"].append(row[5])
                    self.transactions[symbol]["CloseTime"].append(row[6])
                    self.transactions[symbol]["ClosePrice"].append(row[7])
                    self.transactions[symbol]["CloseCommission"].append(row[8])
                    self.transactions[symbol]["Size"].append(row[9])
                    self.transactions[symbol]["Profit"].append(row[10])
            #self.cursor.execute("select * from history where symbol='"+self.sheets[1]+"' order by id desc limit 1")
            #for row in self.cursor.fetchall():
                #if row[7]==0:
                    #if row[1]=='Long':
                        #self.marketPosition[self.sheets[1]]=row[9]
                    #elif row[1]=='Short':
                        #self.marketPosition[self.sheets[1]]=-row[9]
                    #self.transactions[self.sheets[1]]["Type"].append(row[1])
                    #self.transactions[self.sheets[1]]["OpenTime"].append(row[3])
                    #self.transactions[self.sheets[1]]["OpenPrice"].append(row[4])
                    #self.transactions[self.sheets[1]]["OpenCommission"].append(row[5])
                    #self.transactions[self.sheets[1]]["CloseTime"].append(row[6])
                    #self.transactions[self.sheets[1]]["ClosePrice"].append(row[7])
                    #self.transactions[self.sheets[1]]["CloseCommission"].append(row[8])
                    #self.transactions[self.sheets[1]]["Size"].append(row[9])
                    #self.transactions[self.sheets[1]]["Profit"].append(row[10])
        print self.marketPosition
        
    #----------------------------------------------------------------------
    def recvTick(self,jsonVal):
        """接收tick数据"""
        self._updateFigure=0
        sheet1=self.sheets[0]
        sheet2=self.sheets[1]
        if isinstance(jsonVal,dict):
            sheet=jsonVal["Code"]
            if jsonVal["Code"]==sheet1 or jsonVal["Code"]==sheet2:
                dt=datetime.datetime.fromtimestamp(float(jsonVal["QuoteTime"]))
                #如果数据迟到，直接丢弃
                if dt<self.dataQueue[sheet]['datetime'][-1]:
                    return 0
                #将新到的数据存入对应的目录
                self.dataQueue[sheet]['price'].append(float(jsonVal["Last"]))
                self.dataQueue[sheet]['ask'].append(float(jsonVal["Ask1"]))
                self.dataQueue[sheet]['bid'].append(float(jsonVal["Bid1"]))
                self.dataQueue[sheet]['datetime'].append(dt)
                #print sheet,self.dataQueue[sheet]['datetime'][-1],self.dataQueue[sheet]['price'][-1]
                #如果交易时间不匹配，直接丢弃
                #忽略1分钟以内的交易时间不匹配
                if abs(dt-self.dataQueue[sheet1]['datetime'][-1])>=datetime.timedelta(seconds=60) or \
                   abs(dt-self.dataQueue[sheet2]['datetime'][-1])>=datetime.timedelta(seconds=60) or \
                   dt.hour>=15 and dt.hour<20:
                    return 0
                self.dataQueue['datetime'].append(max(self.dataQueue[sheet1]['datetime'][-1],self.dataQueue[sheet2]['datetime'][-1]))
                #如果最新分时==当前分时，修改分时时间和价差；如果最新分时<>当前分时，添加分时时间和价差
                #DataDiff['datetime']用来标识价差对应的时间，也许以后有用                
                if self.dataQueue['datetime'][-1].minute==self.dataDiff['datetime'][-1].minute and \
                   self.dataQueue['datetime'][-1].hour==self.dataDiff['datetime'][-1].hour:
                    self.dataDiff['diff'][-1]=self.dataQueue[sheet1]['price'][-1]-self.dataQueue[sheet2]['price'][-1]
                    if self.dataQueue['datetime'][-1]>self.dataDiff['datetime'][-1]:
                        self.dataDiff['datetime'][-1]=self.dataQueue['datetime'][-1]
                else:
                    self.dataDiff['diff'].append(self.dataQueue[sheet1]['price'][-1]-self.dataQueue[sheet2]['price'][-1])
                    self.dataDiff['datetime'].append(self.dataQueue['datetime'][-1])
                    #每分钟更新价差图
                    self._updateFigure=1
                self.dataQueue['zscore'].append((self.dataDiff['diff'][-1]-np.mean(self.dataDiff['diff'][-self.bandWindow:]))
                                           /np.std(self.dataDiff['diff'][-self.bandWindow:]))
                self.dataQueue['std'].append(np.std(self.dataDiff['diff'][-self.bandWindow:]))
                self.dataQueue['diff'].append(self.dataDiff['diff'][-1])
                print "%s z:%s diff:%s fu:%s td:%s mean:%s std:%s" % (
                    self.dataQueue['datetime'][-1],round(self.dataQueue['zscore'][-1],2),
                    self.dataDiff['diff'][-1],self.dataQueue[sheet1]['price'][-1],self.dataQueue[sheet2]['price'][-1],
                      np.mean(self.dataDiff['diff'][-self.bandWindow:]).round(2),np.std(self.dataDiff['diff'][-self.bandWindow:]).round(2))
                #if self.tradingCount<20:
                self.onBar()
        return self._updateFigure
            
    #----------------------------------------------------------------------
    def onBar(self):
        """检查开平仓情况"""
        pass
            
    #=========================================交易函数=========================================
    def buy(self,symbol,amount,price):
        """现阶段暂无加仓，不能双向开仓
        
        待添加：
        加仓
        """
        if self.marketPosition[symbol]<0:
            self.buyToCover(symbol,price)
        #price+=self.MinMove[symbol]
        self.transactions[symbol]['OpenTime'].append(self.dataQueue['datetime'][-1])
        self.transactions[symbol]['Type'].append('Long')
        self.transactions[symbol]['Size'].append(amount)
        self.transactions[symbol]['OpenPrice'].append(price)
        self.transactions[symbol]['OpenCommission'].append(-abs(amount)*price*self.commission[symbol])
        self.transactions[symbol]['CloseTime'].append(self.dataQueue['datetime'][-1])
        self.transactions[symbol]['ClosePrice'].append(0)
        self.transactions[symbol]['CloseCommission'].append(0)
        self.transactions[symbol]['Profit'].append(0)
        #for item in self.sheets:
            #self.transactions[symbol]['o_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=amount
        self.openConnection()
        sql = """insert into history(type,symbol,openTime,openPrice,openCommission,closeTime,closePrice,closeCommission,size,profit,weiboID)
        values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        param = (
            "Long",symbol,self.dataQueue['datetime'][-1],price,self.transactions[symbol]['OpenCommission'][-1],
            self.transactions[symbol]['CloseTime'][-1],0,0,amount,0,0
            )#int(time.time()))
        n=self.cursor.execute(sql,param)
        self.conn.commit()
        print "buy code:",n
        self.conn.close()
        
    def buyToCover(self,symbol,price):
        self.transactions[symbol]['CloseTime'][-1]=self.dataQueue['datetime'][-1]
        self.transactions[symbol]['ClosePrice'][-1]=price
        self.transactions[symbol]['CloseCommission'][-1]=-abs(self.marketPosition[symbol])*price*self.commission[symbol]
        self.transactions[symbol]['Profit'][-1]=-self.transactions[symbol]['Size'][-1]*(
            price-self.transactions[symbol]['OpenPrice'][-1])+self.transactions[symbol]['CloseCommission'][-1]+ \
            +self.transactions[symbol]['OpenCommission'][-1]
        #for item in self.sheets:
            #self.transactions[symbol]['c_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=0
        #self.balance[symbol].append(self.balance[symbol][-1]+self.transactions[symbol]['Profit'][-1])
        self.openConnection()
        sql="select * from history where symbol='"+symbol+"' order by id desc limit 1"
        self.cursor.execute(sql)
        for row in self.cursor.fetchall():
            tb_id=row[0]
        sql = """update history set closeTime=%s,closePrice=%s,closeCommission=%s,profit=%s where id=%s"""
        param = (
            self.transactions[symbol]['CloseTime'][-1],price,self.transactions[symbol]['CloseCommission'][-1],
            self.transactions[symbol]['Profit'][-1],tb_id
            )#int(time.time()))
        n=self.cursor.execute(sql,param)
        self.conn.commit()
        print "buy code:",n
        self.conn.close()
    
    def sell(self,symbol,price):
        """
        待添加:
        平仓仓位适当性检验
        部分平仓功能
        """
        self.transactions[symbol]['CloseTime'][-1]=self.dataQueue['datetime'][-1]
        self.transactions[symbol]['ClosePrice'][-1]=price
        self.transactions[symbol]['CloseCommission'][-1]=-abs(self.marketPosition[symbol])*price*self.commission[symbol]
        self.transactions[symbol]['Profit'][-1]=self.transactions[symbol]['Size'][-1]*(
            price-self.transactions[symbol]['OpenPrice'][-1])+self.transactions[symbol]['CloseCommission'][-1]+ \
            +self.transactions[symbol]['OpenCommission'][-1]
        #for item in self.sheets:
            #self.transactions[symbol]['c_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=0
        #self.balance[symbol].append(self.balance[symbol][-1]+self.transactions[symbol]['Profit'][-1])
        self.openConnection()
        sql="select * from history where symbol='"+symbol+"' order by id desc limit 1"
        self.cursor.execute(sql)
        for row in self.cursor.fetchall():
            tb_id=row[0]
        sql = """update history set closeTime=%s,closePrice=%s,closeCommission=%s,profit=%s where id=%s"""
        param = (
            self.transactions[symbol]['CloseTime'][-1],price,self.transactions[symbol]['CloseCommission'][-1],
            self.transactions[symbol]['Profit'][-1],tb_id
            )#int(time.time()))
        n=self.cursor.execute(sql,param)
        self.conn.commit()
        print "sell code:",n
        self.conn.close()
    
    def sellshort(self,symbol,amount,price):
        if self.marketPosition[symbol]>0:
            self.sell(symbol,price)
        self.transactions[symbol]['OpenTime'].append(self.dataQueue['datetime'][-1])
        self.transactions[symbol]['Type'].append('Short')
        self.transactions[symbol]['Size'].append(amount)
        self.transactions[symbol]['OpenPrice'].append(price)
        self.transactions[symbol]['OpenCommission'].append(-abs(amount)*price*self.commission[symbol])
        self.transactions[symbol]['CloseTime'].append(self.dataQueue['datetime'][-1])
        self.transactions[symbol]['ClosePrice'].append(0)
        self.transactions[symbol]['CloseCommission'].append(0)
        self.transactions[symbol]['Profit'].append(0)
        #for item in self.sheets:
            #self.transactions[symbol]['o_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=-amount
        self.openConnection()
        sql = """insert into history(type,symbol,openTime,openPrice,openCommission,closeTime,closePrice,closeCommission,size,profit,weiboID) 
        values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        param = (
            "Short",symbol,self.dataQueue['datetime'][-1],price,self.transactions[symbol]['OpenCommission'][-1],
            self.transactions[symbol]['CloseTime'][-1],0,0,amount,0,0
            )
        n=self.cursor.execute(sql,param)
        self.conn.commit()
        print "sellshort code:",n
        self.conn.close()
        
    #----------------------------------------------------------------------
    def sellSpread(self,leg1,leg2):
        """"""
        self.sellshort(leg1[0],leg1[1],leg1[2])
        self.buy(leg2[0],leg2[1],leg2[2])
        self.tradingCount+=1
        
    #----------------------------------------------------------------------
    def buySpread(self,leg1,leg2):
        """"""
        self.buy(leg1[0],leg1[1],leg1[2])
        self.sellshort(leg2[0],leg2[1],leg2[2])
        self.tradingCount+=1
        
    #----------------------------------------------------------------------
    def closeLong(self,leg1,leg2):
        """"""
        self.sell(leg1[0],leg1[1])
        self.buyToCover(leg2[0],leg2[1])
        self.tradingCount+=1
        
    #----------------------------------------------------------------------
    def closeShort(self,leg1,leg2):
        """"""
        self.buyToCover(leg1[0],leg1[1])
        self.sell(leg2[0],leg2[1])
        self.tradingCount+=1
        
  