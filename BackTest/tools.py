#-*- coding: utf-8 -*-
#BackTest工具函数集
#4.8 Powered by hhhcj

import pandas as pd
from pandas.io.excel import *
import rwExcel

#----------------------------------------------------------------------
def prepareData(source,dest,sheet1):
    """将数据源保存为DataFrame格式"""
    rw=rwExcel.rwExcel(source)
    temp1=rw.getRange(sheet1)
    rw.close()
    data1=[i[1] for i in temp1]
    #针对excel时间列不是标准时间字符串的情况，将其它转化成字符串
    if isinstance(temp1[0][0],str):
        ind1=pd.DatetimeIndex([i[0] for i in temp1])
    else:
        ind1=pd.DatetimeIndex([str(i[0]) for i in temp1])
    fu=pd.DataFrame(data=data1,index=ind1,columns=[sheet1])
    writer=ExcelWriter(dest)
    fu.to_excel(writer,sheet_name=sheet1)
    writer.save()

def prepareArbitrageData(dest,sheet,period=None):
    try:
        rw=rwExcel.rwExcel(dest[0])
        temp1=rw.getRange(sheet[0])
        temp2=rw.getRange(sheet[1])
        rw.close()
        data1=[i[1] for i in temp1]
        data2=[i[1] for i in temp2]
        #针对excel时间列不是标准时间字符串的情况，将其它转化成字符串
        if isinstance(temp1[0][0],str):
            ind1=pd.DatetimeIndex([i[0] for i in temp1])
        else:
            ind1=pd.DatetimeIndex([str(i[0]) for i in temp1])
        if isinstance(temp2[0][0],str):
            ind2=pd.DatetimeIndex([i[0] for i in temp2])
        else:
            ind2=pd.DatetimeIndex([str(i[0]) for i in temp2])
        fu=pd.DataFrame(data=data1,index=ind1,columns=[sheet[0]])
        td=pd.DataFrame(data=data2,index=ind2,columns=[sheet[1]])
        if lastPeriod is not None:
            fu=fu[fu.index>=period[0] and fu.index<=period[1]]
            #td=td[td.index>=period[0] and td.index<=period[1]]
        ind=ind1.intersection(ind2)
        fu=fu.ix[ind]
        td=td.ix[ind]
        fu=fu.join(td)
        writer=ExcelWriter(dest)
        fu.to_excel(writer,sheet_name=sheet1+sheet2)
        #td.to_excel(writer,sheet_name=sheet2,header=False)
        writer.save()
        return fu
    except pythoncom.com_error as (hr, msg, exc, arg):  
        print "The Excel call failed with code %d: %s" % (hr, msg)
        if exc is None:
            print "There is no extended error information"
        else:
            wcode, source, text, helpFile, helpId, scode = exc
            print "The source of the error is", source
            print "The error message is", text
            print "More info can be found in %s (id=%d)" % (helpFile, helpId)
        rw.close()
        
        
########################################################################         
def permutations(*lists):
    #list all possible composition from many list, each item is a tuple.
    #Here lists is [list1, list2, list3], return a list of [(item1,item2,item3),...]
 
    #len of result list and result list.
    total = reduce(lambda x, y: x * y, map(len, lists))
    retList = []
 
    #every item of result list.
    for i in range(0, total):
        step = total
        tempItem = []
        for l in lists:
            step /= len(l)
            tempItem.append(l[i/step % len(l)])
        retList.append(tuple(tempItem))
 
    return retList        

######################################################################## 