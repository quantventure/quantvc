#-*- coding: utf-8 -*-
#1.3 Powered by hhhcj
import numpy as np
import os
from win32com import client

class rwExcel:
    def __init__(self, filename=None):
        self.xlApp = client.Dispatch('Excel.Application')
        if filename:
            self.filename = filename
            self.xlBook = self.xlApp.Workbooks.Open(os.path.join(os.getcwd(),filename))
            self.rowNum=1
            self.colNum=1
        else:
            self.xlBook = self.xlApp.Workbooks.Add()
            self.filename = filename
        self.open=True
          
    #忘记close后,程序退出时,自动close
    def __del__(self):
        if self.open:
            self.close()
            
    def close(self):
        self.xlBook.Close(SaveChanges=0)
        del self.xlApp
        self.open=False
                    
    def getCell(self, sheet, row, col):
        #Get value of one cell
        return self.xlBook.Worksheets(sheet).Cells(row, col).Value
    
    def getRange(self, sheet, range2=''):
        if range2:
            return self.xlBook.Worksheets(sheet).Range(range2).Value
        else:
            return self.xlBook.Worksheets(sheet).UsedRange.Value

    def getRowsCount(self,sheet):
        return self.xlBook.Worksheets(sheet).UsedRange.Rows.Count

    def setCell(self, sheet, row, col, value):
        #set value of one cell
        self.xlBook.Worksheets(sheet).Cells(row, col).Value = value
        
    def setRange(self, sheet=None, range2=None, value='空白'):
        if sheet is None:
            sheet=1
        if range2:
            self.xlBook.Worksheets(sheet).Range(range2).Value =value
        else:
            #if isinstance(value,np.ndarray):
                #self.xlBook.Worksheets(sheet).Range("a1:"+chr(value.ndim+64)+str(value.size/value.ndim)).Value=value
            i=j=0
                #if value.ndim==1:
                    #for row in value:
                        #i+=1
                        #self.xlBook.Worksheets(sheet).Cells(i,1).Value=row
                #elif value.ndim==2:
            for row in value:
                    i+=1
                    #print row
                    for col in row:
                        j+=1
                        #print
                        self.xlBook.Worksheets.Add
                        self.xlBook.ActiveSheet.Name=sheet
                        self.xlBook.Worksheets(sheet).Cells(i,j).Value=col
                #else:
                    #raise "1<=ndarray.ndim<=2!!"
            #else:
                #print "need ndarray"
                                                
            
    def save(self, newfilename=None):
        if newfilename:
            self.filename = newfilename
            self.xlBook.SaveAs(os.path.join(os.getcwd(),newfilename))
        else:
            self.xlBook.Save()
            