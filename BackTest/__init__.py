#-*- coding: utf-8 -*-
#BackTest函数集
#1.8 Powered by hhhcj

from collections import deque
import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas.io.excel import *
import pythoncom
import rwExcel
from talib import func

#----------------------------------------------------------------------
def prepareData(source,dest,sheet1):
    """将数据源保存为DataFrame格式"""
    rw=rwExcel.rwExcel(source)
    temp1=rw.getRange(sheet1)
    rw.close()
    data1=[i[1] for i in temp1]
    #针对excel时间列不是标准时间字符串的情况，将其它转化成字符串
    if isinstance(temp1[0][0],str):
        ind1=pd.DatetimeIndex([i[0] for i in temp1])
    else:
        ind1=pd.DatetimeIndex([str(i[0]) for i in temp1])
    fu=pd.DataFrame(data=data1,index=ind1,columns=[sheet1])
    writer=ExcelWriter(dest)
    fu.to_excel(writer,sheet_name=sheet1)
    writer.save()

def prepareArbitrageData(source,dest=None,sheet=None,period=None):
    try:
        temp={}
        data={}
        ind=pd.DatetimeIndex([])
        rw=rwExcel.rwExcel(source)
        for symbol in sheet:
            temp[symbol]=rw.getRange(symbol)
        rw.close()
        for symbol in sheet:
            data[symbol]=[i[1] for i in temp[symbol]]
            #针对excel时间列不是标准时间字符串的情况，将其它转化成字符串
            if isinstance(temp[symbol][0][0],str):
                ind=pd.DatetimeIndex([i[0] for i in temp[symbol]])
            else:
                ind=pd.DatetimeIndex([str(i[0]) for i in temp[symbol]])
            data[symbol]=pd.DataFrame(data=data[symbol],index=ind,columns=[symbol])
            if period is not None:
                ind=ind[ind>=period[0]]
                ind=ind[ind<=period[1]]
                data[symbol]=data[symbol].ix[ind]
        for symbol in sheet:
            ind=ind.intersection(data[symbol].index)
        df=data[sheet[0]].ix[ind]
        for symbol in sheet:
            if symbol<>sheet[0]:
                df=df.join(data[symbol].ix[ind])
        if dest is not None:
            df.to_csv(dest)
        return df
    except pythoncom.com_error as (hr, msg, exc, arg):  
        print "The Excel call failed with code %d: %s" % (hr, msg)
        if exc is None:
            print "There is no extended error information"
        else:
            wcode, source, text, helpFile, helpId, scode = exc
            print "The source of the error is", source
            print "The error message is", text
            print "More info can be found in %s (id=%d)" % (helpFile, helpId)
        rw.close()
        

#----------------------------------------------------------------------
def prepareExchangeRate(source,dest,sheets):
    """踢除数据的汇率因素，转换第二列

    source：原始数据和汇率数据
    dest：汇率转换后的路径
    sheet：原始数据列列名
    """
    rw=rwExcel.rwExcel(source[0])
    temp=rw.getRange(sheet[0])
    rw.close()
    data=[i[1] for i in temp]
    #针对excel时间列不是标准时间字符串的情况，将其它转化成字符串
    if isinstance(temp[0][0],str):
        ind=pd.DatetimeIndex([i[0] for i in temp])
    else:
        ind=pd.DatetimeIndex([str(i[0]) for i in temp])
    usdcny=pd.Series(data=data,index=ind)
    xls=ExcelFile(source[1])
    df=xls.parse(sheets[0]+sheets[1])
    i=0
    start=usdcny[i]
    for item in df.index:
        if item.date==ind[i].date:
            df[item][sheets[1]]
    
        
        
########################################################################         
def permutations(*lists):
    #list all possible composition from many list, each item is a tuple.
    #Here lists is [list1, list2, list3], return a list of [(item1,item2,item3),...]
 
    #len of result list and result list.
    total = reduce(lambda x, y: x * y, map(len, lists))
    retList = []
 
    #every item of result list.
    for i in range(0, total):
        step = total
        tempItem = []
        for l in lists:
            step /= len(l)
            tempItem.append(l[i/step % len(l)])
        retList.append(tuple(tempItem))
 
    return retList        

########################################################################    
class BackTest(object):
    """回溯测试类库"""
    
    def __init__(self,dataInput,sheets,sheetname=None,commission=0.5,commissionType='relative',equity=50,swap=0):
        """
        commission=0.5%%
        commissionType=('relative','absolute')
        
        待添加：
        对Open,High,Low,Close的支持
        """
        self.balance={}#对应品种的余额
        self.commission={}
        self.marketPosition={}
        self.transactions={}
        self.sheetName=sheets
        self.params=[]
        self.param=None
        self.optRecords={}
        self.MinMove={}
        
        #rw=rwExcel.rwExcel(dataInput)
        #xls=ExcelFile(dataInput)
        #if sheetname is None:
            #self.sheets=pd.read_csv(sheets[0]+sheets[1],index_col=0)
        #else:
        self.sheets=pd.read_csv(dataInput,index_col=0,parse_dates=True)
        for symbol in sheets:
            self.commission[symbol]=commission
            self.balance[symbol]=[equity]
            self.marketPosition[symbol]=0
            self.MinMove[symbol]=0.0
            self.sheets[symbol+'Equity']=np.nan
            self.sheets[symbol+'MarginLevel']=0.0
            self.sheets[symbol+'Position']=np.nan
            self.transactions[symbol]={"OpenTime":[],'Type':[],'Size':[],'OpenPrice':[],'OpenCommission':[],
                                      'CloseTime':[],'ClosePrice':[],'CloseCommission':[],'Profit':[]}
        self.sheets['portfolioEquity']=0.0
        self.sheets['marginLevel']=0.0
        self.sheets['datetime']=self.sheets.index
        self.sheets.index=range(len(self.sheets['datetime']))
        
        self.bar=Bars(self.sheets)
        self.onOptInit()
        if len(self.params)==0:
            print 'Initializing...'
            self.onInit()
            for symbol in sheets:
                for item in self.sheets:
                    self.transactions[symbol]['o_'+item]=[]
                    self.transactions[symbol]['c_'+item]=[]
            print u'［收益，盈亏比，年化收益率，最大回撤，收益风险比]'
            self._start()
            #保存交易记录
            sheet1=self.sheetName[0]
            a=pd.DataFrame(self.transactions[sheet1])
            a['Symbol']=sheet1
            for symbol in self.sheetName:
                if symbol<>sheet1:
                    b=pd.DataFrame(self.transactions[symbol])
                    b['Symbol']=symbol
                    a=a.append(b)
            self.transactions=a.sort_index()
            #if len(self.sheetName)==1:
                #sheet1=self.sheetName[0]
                #self.transactions=pd.DataFrame(self.transactions[self.sheetName[0]])
                #self.transactions['Symbol']=self.sheetName[0]
            #elif len(self.sheetName)==2:
                #sheet1=self.sheetName[0]
                #sheet2=self.sheetName[1]
                #a=pd.DataFrame(self.transactions[sheet1])
                #a['Symbol']=sheet1
                #b=pd.DataFrame(self.transactions[sheet2])
                #b['Symbol']=sheet2
                #self.transactions=a.append(b).sort_index()
            #temp=(pd.to_datetime(self.transactions['CloseTime'])-pd.to_datetime(self.transactions['OpenTime'])).values
            #self.transactions['Period']=[i/24/3600/1000000000 for i in temp]
            self.transactions.to_excel('data\\TradingHistory.xlsx',columns=['Type','Symbol','OpenTime','OpenPrice','OpenCommission',
                                                                         'o_diff','o_mid','o_std','o_zscore',
                                                                         'CloseTime','ClosePrice','CloseCommission',
                                                                         'c_diff','c_mid','c_std','c_zscore',
                                                                         'Size','Profit'])#,'Period'
            self.onStop()
        else:
            print u'［收益，盈亏比，收益风险比]'
            for item in self.params:
                self.param=item
                self.onInit()
                self._start()
                #下次回测开始前初始值清零
                for symbol in sheets:
                    self.commission[symbol]=commission
                    self.balance[symbol]=[equity]
                    self.marketPosition[symbol]=0
                    self.sheets[symbol+'Equity']=np.nan
                    self.sheets[symbol+'MarginLevel']=0.0
                    self.sheets[symbol+'Position']=np.nan
                    self.transactions[symbol]={"OpenTime":[],'Type':[],'Size':[],'OpenPrice':[],'OpenCommission':[],
                                              'CloseTime':[],'ClosePrice':[],'CloseCommission':[],'Profit':[]}
                self.sheets['portfolioEquity']=0.0
                self.sheets['marginLevel']=0.0
            #保留优化记录
            self.optRecords=pd.DataFrame(data=self.optRecords.values(),
                                       index=[str(i) for i in self.optRecords],
                                       columns=[u'收益',u'盈亏比',u'收益风险比'])
            self.optRecords.to_excel('data\\optRecords.xlsx',cols=[u'收益',u'盈亏比',u'收益风险比'])
        self.onOptStop()
        
    #=========================================内置函数=========================================
    def _init(self):
        """"""
        self.onInit()
        # 这一步须写在onStart前面，以确保self.bar能初始化用户自定义的指标序列
        self.bar=Bars(self.sheets)
    
    def _start(self):
        for symbol in self.commission:
            self.commission[symbol]*=.0001
            self.balance[symbol][0]*=10000
        #进入事件循环
        for i in self.sheets.index:
            self.bar.setCounter(i=i)
            #onBar之前更新净值
            for symbol in self.marketPosition:
                if self.marketPosition[symbol]!=0:
                    self.sheets.loc[i,symbol+'Equity']=self.balance[symbol][-1]+self.marketPosition[symbol]* \
                                               (self.sheets[symbol][i]-self.transactions[symbol]['OpenPrice'][-1])
                    self.sheets.loc[i,symbol+'MarginLevel']=self.sheets[symbol+'Equity'][i]/ \
                                                     (abs(self.marketPosition[symbol])*self.sheets[symbol][i])
                else:
                    self.sheets.loc[i,symbol+'Equity']=self.balance[symbol][-1]
                    self.sheets.loc[i,symbol+'MarginLevel']=np.nan
                #记录仓位
                self.sheets.loc[i,symbol+'Position']=self.marketPosition[symbol]
                #添加净值和保证金率到self.sheets
                self.sheets.loc[i,'portfolioEquity']+=self.sheets[symbol+'Equity'][i]
                self.sheets.loc[i,'marginLevel']+=abs(self.sheets[symbol+'Position'][i])*self.sheets[symbol][i]
            self.sheets.loc[i,'marginLevel']=self.sheets['portfolioEquity'][i]/self.sheets['marginLevel'][i]
            self.onBar()
        #将未平仓部分平仓，closeAtStop
        for symbol in self.transactions:
            if len(self.transactions[symbol]['ClosePrice'])>0 and self.transactions[symbol]['ClosePrice'][-1]==0:
                if self.transactions[symbol]['Type'][-1]=='Long':
                    self.sell(symbol, self.bar[symbol][0])
                elif self.transactions[symbol]['Type'][-1]=='Short':
                    self.buyToCover(symbol, self.bar[symbol][0])
        #统计收益、盈亏比、收益风险比
        profit,loss,temp2=0,0,0
        sheet1=self.sheetName[0]
        for i in range(len(self.transactions[sheet1]['OpenTime'])):
            temp2=0
            for symbol in self.sheetName:
                temp2+=self.transactions[symbol]['Profit'][i]
            if temp2>0:
                profit+=temp2
            else:
                loss+=temp2
        #if len(self.sheetName)==1:
            #sheet1=self.sheetName[0]
            #for i in range(len(self.transactions[sheet1]['OpenTime'])):
                #temp2=self.transactions[sheet1]['Profit'][i]
                #if self.transactions[sheet1]['Profit'][i]>0:
                    #profit+=temp2
                #else:
                    #loss+=temp2
        #elif len(self.sheetName)==2:
            #sheet1=self.sheetName[0]
            #sheet2=self.sheetName[1]
            #for i in range(len(self.transactions[sheet1]['OpenTime'])):
                #temp2=self.transactions[sheet1]['Profit'][i]+self.transactions[sheet2]['Profit'][i]
                #if temp2>0:
                    #profit+=temp2
                #else:
                    #loss+=temp2
        i=0
        Peak=0
        MaxBack=0
        while i<len(self.sheets['portfolioEquity']):
            if self.sheets['portfolioEquity'][i]>Peak:
                Peak=self.sheets['portfolioEquity'][i]
            back=Peak-self.sheets['portfolioEquity'][i]
            if back>MaxBack:
                MaxBack=back
            i+=1
        if self.param is not None:
            self.optRecords[self.param]=[profit+loss, \
                                         np.nan if loss==0 else abs(profit/loss), \
                                         np.nan if MaxBack==0 else (profit+loss)/MaxBack]
            print str(self.param)+':'+str(self.optRecords[self.param])
        else:
            earnings=profit+loss
            period=self.sheets['datetime'][len(self.sheets)-1]-self.sheets['datetime'][0]
            #period=datetime.timedelta(self.sheets['datetime'].last-self.sheets['datetime'].first/1000)
            returns=earnings/self.sheets['portfolioEquity'][0]/period.days
            print '[%.2f, %.2f, %.2f%%, %.2f,%.2f]' % (earnings, \
                   np.nan if loss==0 else abs(profit/loss), \
                   returns*100, \
                   MaxBack, \
                   np.nan if MaxBack==0 else returns*self.sheets['portfolioEquity'][0]/MaxBack)
    
    def draw(self,*args):
        """画图会阻塞程序继续运行，关闭图像后程序直接退出，有选项让它并行吗？
        
        待添加：
        自动将X轴换成时间
        """
        for data in args:
            plt.plot(data)#self.sheets['datetime'],
    
    def report(self):
        """显示交易记录
        
        bug:
        OpenCommission、CloseCommission显示的太长
        """
        for symbol in self.transactions:
            disp=zip(self.transactions[symbol]['OpenTime'],self.transactions[symbol]['Type'],self.transactions[symbol]['Size'],
                     self.transactions[symbol]['OpenPrice'],self.transactions[symbol]['OpenCommission'],
                     self.transactions[symbol]['CloseTime'],self.transactions[symbol]['ClosePrice'],
                     self.transactions[symbol]['CloseCommission'],self.transactions[symbol]['Profit'])
            print 'OpenTime '+'Type '+'Size '+'OpenPrice '+'OpenCommission '+ \
                  'CloseTime '+'ClosePrice '+"CloseCommission "+'Profit '
            for item in disp:
                print item
    
    #=========================================事件函数=========================================
    def onBar(self):
        """需要被重写"""
        pass
        
    def onInit(self):
        """初始化交易环境"""
        """指标和其它自定义的变量的初始化写在这里"""
        pass
    
    #----------------------------------------------------------------------
    def onOptInit(self):
        """使用permutations初始化优化参数params"""
        """不进行优化操作，不要重载该方法"""
        pass
    
    #----------------------------------------------------------------------
    def onOptStop(self):
        """操作optRecords的相关内容"""
        """不进行优化操作，不要重载该方法"""
        pass
    
    def onStart(self):
        """自定义Bar后，保留事件"""
        pass
    
    def onStop(self):
        """回测结束后的处理，如保存记录、画图等"""
        pass
    
    #=========================================交易函数=========================================
    def buy(self,symbol,amount,price):
        """现阶段暂无加仓，不能双向开仓
        
        待添加：
        加仓
        """
        if self.marketPosition[symbol]<0:
            self.buyToCover(symbol,price)
        price+=self.MinMove[symbol]
        self.transactions[symbol]['OpenTime'].append(self.bar.datetime)
        self.transactions[symbol]['Type'].append('Long')
        self.transactions[symbol]['Size'].append(amount)
        self.transactions[symbol]['OpenPrice'].append(price)
        self.transactions[symbol]['OpenCommission'].append(-abs(amount)*price*self.commission[symbol])
        self.transactions[symbol]['CloseTime'].append(self.bar.datetime)
        self.transactions[symbol]['ClosePrice'].append(0)
        self.transactions[symbol]['CloseCommission'].append(0)
        self.transactions[symbol]['Profit'].append(0)
        for item in self.sheets.columns:
            self.transactions[symbol]['o_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=amount
        
    def buyToCover(self,symbol,price):
        price+=self.MinMove[symbol]
        self.transactions[symbol]['CloseTime'][-1]=self.bar.datetime
        self.transactions[symbol]['ClosePrice'][-1]=price
        self.transactions[symbol]['CloseCommission'][-1]=-abs(self.marketPosition[symbol])*price*self.commission[symbol]
        self.transactions[symbol]['Profit'][-1]=-self.transactions[symbol]['Size'][-1]*(
            price-self.transactions[symbol]['OpenPrice'][-1])+self.transactions[symbol]['CloseCommission'][-1]+ \
            +self.transactions[symbol]['OpenCommission'][-1]
        for item in self.sheets:
            self.transactions[symbol]['c_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=0
        self.balance[symbol].append(self.balance[symbol][-1]+self.transactions[symbol]['Profit'][-1])
    
    def sell(self,symbol,price):
        """
        待添加:
        平仓仓位适当性检验
        部分平仓功能
        """
        price-=self.MinMove[symbol]
        self.transactions[symbol]['CloseTime'][-1]=self.bar.datetime
        self.transactions[symbol]['ClosePrice'][-1]=price
        self.transactions[symbol]['CloseCommission'][-1]=-abs(self.marketPosition[symbol])*price*self.commission[symbol]
        self.transactions[symbol]['Profit'][-1]=self.transactions[symbol]['Size'][-1]*(
            price-self.transactions[symbol]['OpenPrice'][-1])+self.transactions[symbol]['CloseCommission'][-1]+ \
            +self.transactions[symbol]['OpenCommission'][-1]
        for item in self.sheets:
            self.transactions[symbol]['c_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=0
        self.balance[symbol].append(self.balance[symbol][-1]+self.transactions[symbol]['Profit'][-1])
    
    def sellshort(self,symbol,amount,price):
        if self.marketPosition[symbol]>0:
            self.sell(symbol,price)
        price-=self.MinMove[symbol]
        self.transactions[symbol]['OpenTime'].append(self.bar.datetime)
        self.transactions[symbol]['Type'].append('Short')
        self.transactions[symbol]['Size'].append(amount)
        self.transactions[symbol]['OpenPrice'].append(price)
        self.transactions[symbol]['OpenCommission'].append(-abs(amount)*price*self.commission[symbol])
        self.transactions[symbol]['CloseTime'].append(self.bar.datetime)
        self.transactions[symbol]['ClosePrice'].append(0)
        self.transactions[symbol]['CloseCommission'].append(0)
        self.transactions[symbol]['Profit'].append(0)
        for item in self.sheets:
            self.transactions[symbol]['o_'+item].append(self.sheets[item][self.bar.i])
        self.marketPosition[symbol]=-amount
    
    def sendOrder(self,symbol,amount,price):
        #针对有加仓的情况，平仓先从老的开始；有平仓才有开仓。
        #if len(self._position[symbol])==0 or self.transactions['Size'][len(self._position[symbol])]*amount>0:
        #if len(self.transactions['OpenTime'])==0 or self.transactions['Size'][len(self._position[symbol])]*amount>0:
        self.transactions['OpenTime'].append(self.bar.datetime)
        if amount>0:
            self.transactions['Type'].append('Long')
        elif amount<0:
            self.transactions['Type'].append('Short')
        self.transactions['Symbol'].append(symbol)
        self.transactions['Size'].append(amount)
        self.transactions['OpenPrice'].append(price)
        self.transactions['OpenCommission'].append(-abs(amount)*price*self.commission[symbol])
        #self._equity[symbol][-1]-=self.transactions['OpenCommission'][-1]
        #self.position[symbol]+=amount
            #self.transactions['CloseTime'].append(self.bar.datetime)
            #self.transactions['ClosePrice'].append(0)
            #self.transactions['CloseCommission'].append(0)
        #else :
            #self.transactions['CloseTime'][-1]=self.bar.datetime
            #self.transactions['ClosePrice'][-1]=price
            #self.transactions['CloseCommission'][-1]=abs(amount)*price*self.commission[symbol]
            
        if len(self._position[symbol]['Size'])==0 or self._position[symbol]['Size'][0]*amount>0:
            self._position[symbol]['Size'].append(amount)
            self._position[symbol]['Price'].append(price)
            self.balance[symbol].append(self.balance[symbol][-1]+self.transactions['OpenCommission'][-1])
        else:
            temp=amount+self._position[symbol]['Size'][0]
            while len(self._position[symbol]['Size'])>1 and temp*amount>=0:
                self._position[symbol]['Size'].popleft()
                self._position[symbol]['Price'].popleft()
                temp+=self._position[symbol]['Size'][0]
            self._position[symbol]['Size'][0]=temp
                
########################################################################
class Bars(object):
    """重写__getitem__方法，重新包装self.sheets的检索方法"""
    
    def __init__(self,sheets):
        if isinstance(sheets,pd.DataFrame): 
            self.sheets=sheets
            self.setCounter()
            #self.i=0
            #self.datetime=sheets['datetime'][0]
        
    def __getitem__(self,symbol):
        if symbol in self.sheets:
            return _Bar(self,symbol)
    
    def setCounter(self,i=0):
        self.i=i
        self.datetime=self.sheets['datetime'][self.i]
                
########################################################################
class _Bar(object):
    def __init__(self,bars,symbol):#[]*args
        if isinstance(bars,Bars):
            self.bars=bars
            self.symbol=symbol
        
    def __getitem__(self,k):
        if self.bars.i-k>=0:
            return self.bars.sheets[self.symbol][self.bars.i-k]
        else:
            return self.bars.sheets[self.symbol][0]
        

########################################################################
class BSTools:
    """工具箱"""

    #----------------------------------------------------------------------
    def __init__(self,dataInput,sheets,sheetname=None):
        """Constructor"""
        self.sheetname=sheets
        xls=ExcelFile(dataInput)
        if sheetname is None:
            self.sheets=xls.parse(sheets[0]+sheets[1])
        else:
            self.sheets=xls.parse(sheetname)
    
    #----------------------------------------------------------------------
    def zscoreCount(self):
        """zcore统计"""
        self.sheets['diff']=self.sheets[self.sheetname[0]]-self.sheets[self.sheetname[1]]
        #spread=self.sheets[sheet1]-self.sheets[sheet2]
        self.sheets['upper'],self.sheets['mid'],self.sheets['lower']=func.BBANDS(
            self.sheets['diff'],timeperiod=545)#,471,matype=MA_Type.WMA
        self.sheets['zscore'] = (self.sheets['diff'] - self.sheets['mid'])*4 / (self.sheets['upper']-self.sheets['lower'])
        #print self.sheets['zscore'][abs(self.sheets['zscore'])>2].dropna().count()
        print 'zscore>2:\t%.2f%%' % (
              float(self.sheets['zscore'][abs(self.sheets['zscore'])>2].dropna().count()*100)/len(self.sheets['zscore']))
        print 'zscore>3:\t%.2f%%' % (
              float(self.sheets['zscore'][abs(self.sheets['zscore'])>3].dropna().count()*100)/len(self.sheets['zscore']))
        print 'zscore>4:\t%.2f%%' % (
              float(self.sheets['zscore'][abs(self.sheets['zscore'])>4].dropna().count()*100)/len(self.sheets['zscore']))
        
    #----------------------------------------------------------------------
    def diffCount(self):
        """"""
        pass
        
            
            
        
########################################################################
class BTReport:
    """交易记录分析"""

    #----------------------------------------------------------------------
    def __init__(self,source):
        """
        path:交易记录
        """
        if isinstance(source,pd.DataFrame):
            self.transactions=source
        elif isinstance(source,str):
            xls=ExcelFile(source)
            self.transactions=xls.parse('Sheet1')
        else:
            print '没有交易记录！'
        self.count=len(self.transactions['CloseTime'][0])
        
    #----------------------------------------------------------------------
    def periodReport(self,visible=True):
        """持仓周期分析
        
        平均持仓周期、持仓周期中位数、最大持仓周期、最小持仓周期
        """
        period=pd.to_datetime(self.transactions['CloseTime'])-pd.to_datetime(self.transactions['OpenTime'])
        if len(period)==0:
            print u'没有交易记录'
            return
        print '='*20
        #print period.mean()/1000
        #print period.mean()
        print u'平均持仓周期:%s\n持仓周期中位数:%s\n最大持仓周期:%s\n最小持仓周期:%s' % (
            period.mean(),period.median(),period.max(),period.min())
            #datetime.timedelta(microseconds=period.mean()/1000),
            #datetime.timedelta(microseconds=period.median()/1000),
            #datetime.timedelta(microseconds=period.max().item(0)/1000),
            #datetime.timedelta(microseconds=period.min().item(0)/1000))
        #开平仓时间分析
        freq1=[0]*96
        freq1_0=[0]*96
        freq2=[0]*96
        freq2_0=[0]*96
        freq3=[0]*96
        freq3_0=[0]*96
        freq4=[0]*96
        freq4_0=[0]*96
        freq5=[0]*96
        freq5_0=[0]*96
        freq6=[0]*96
        freq6_0=[0]*96
        winPeriod=[]
        lossPeriod=[]
        winCount=0
        totalCount=0
        for i in range(len(self.transactions['CloseTime'])/self.count):
            #to_datetime for standlize timezone
            openTime=pd.to_datetime(self.transactions['OpenTime'].values[self.count*i])
            closeTime=pd.to_datetime(self.transactions['CloseTime'].values[self.count*i])
            freq5[openTime.hour*4+int(openTime.minute/15)]+=1
            freq6[closeTime.hour*4+int(closeTime.minute/15)]+=1
            if self.transactions['Profit'].values[self.count*i]+self.transactions['Profit'].values[self.count*i+1]>=0:
                freq1[openTime.hour*4+int(openTime.minute/15)]+=1
                freq2[closeTime.hour*4+int(closeTime.minute/15)]+=1
                winPeriod.append(closeTime-openTime)
                winCount+=1
            else:
                freq3[openTime.hour*4+int(openTime.minute/15)]+=1
                freq4[closeTime.hour*4+int(closeTime.minute/15)]+=1
                lossPeriod.append(closeTime-openTime)
            totalCount+=1
        winPeriod=pd.Series(winPeriod)
        lossPeriod=pd.Series(lossPeriod)
        if len(winPeriod)==0 or len(lossPeriod)==0:
            print u'没有盈亏记录'
            return
        print '='*20
        print u'平均盈利持仓周期:%s\n盈利持仓周期中位数:%s\n最大盈利持仓周期:%s\n最小盈利持仓周期:%s' % (
            period.mean(),period.median(),period.max(),period.min())
            #datetime.timedelta(microseconds=winPeriod.mean()/1000),#period.max().item,period.min())
            #datetime.timedelta(microseconds=winPeriod.median()/1000),
            #datetime.timedelta(microseconds=winPeriod.max().item(0)/1000),
            #datetime.timedelta(microseconds=winPeriod.min().item(0)/1000))
        print '='*20
        print u'平均亏损持仓周期:%s\n亏损持仓周期中位数:%s\n最大亏损持仓周期:%s\n最小亏损持仓周期:%s' % (
            period.mean(),period.median(),period.max(),period.min())
            #datetime.timedelta(microseconds=lossPeriod.mean()/1000),#period.max().item,period.min())
            #datetime.timedelta(microseconds=lossPeriod.median()/1000),
            #datetime.timedelta(microseconds=lossPeriod.max().item(0)/1000),
            #datetime.timedelta(microseconds=lossPeriod.min().item(0)/1000))
        print '='*20
        print u'盈利交易:%s 亏损交易:%s 胜率:%.2f%%' % (winCount,totalCount-winCount,round(winCount*100.0/totalCount,2))
        freq1_0[10:36]=[max(freq1)]*26
        freq1_0[62:80]=[max(freq1)]*18
        freq2_0[10:36]=[max(freq2)]*26
        freq2_0[62:80]=[max(freq2)]*18
        freq3_0[10:36]=[max(freq3)]*26
        freq3_0[62:80]=[max(freq3)]*18
        freq4_0[10:36]=[max(freq4)]*26
        freq4_0[62:80]=[max(freq4)]*18
        freq5_0[10:36]=[max(freq5)]*26
        freq5_0[62:80]=[max(freq5)]*18
        freq6_0[10:36]=[max(freq6)]*26
        freq6_0[62:80]=[max(freq6)]*18
        if visible is True:
            plt.figure()
            ax6=plt.subplot(411,ylabel='Profit OpenTime')
            ax6.bar(range(96),freq1)
            ax6.bar(range(96),freq1_0,color='y',edgecolor='y')
            ax7=plt.subplot(412,ylabel='Profit CloseTime')#,sharex=ax6
            ax7.bar(range(96),freq2)
            ax7.bar(range(96),freq2_0,color='y',edgecolor='y')
            ax8=plt.subplot(413,ylabel='Loss OpenTime')#,sharex=ax6
            ax8.bar(range(96),freq3)
            ax8.bar(range(96),freq3_0,color='y',edgecolor='y')
            ax9=plt.subplot(414,ylabel='Loss CloseTime')#,sharex=ax6
            ax9.bar(range(96),freq4)
            ax9.bar(range(96),freq4_0,color='y',edgecolor='y')
            plt.figure()
            ax1=plt.subplot(211,ylabel='OpenTime')#,sharex=ax6
            ax1.bar(range(96),freq5)
            ax1.bar(range(96),freq5_0,color='y',edgecolor='y')
            ax2=plt.subplot(212,ylabel='CloseTime')#,sharex=ax6
            ax2.bar(range(96),freq6)
            ax2.bar(range(96),freq6_0,color='y',edgecolor='y')
        
    #----------------------------------------------------------------------
    def commissionReport(self):
        """手续费分析
        
        开仓手续费、平仓手续费、手续费利润比
        """
        commission1=self.transactions['OpenCommission'].sum()
        commission2=self.transactions['CloseCommission'].sum()
        profit=self.transactions['Profit'].sum()
        if profit==0:
            print u'没有利润！'
            return
        print '='*20
        print u'开仓手续费比率:%.f%% 平仓手续费比率:%.f%% 总手续费利润比:%.f%%' % (
            -commission1*100/profit,-commission2*100/profit,-(commission1+commission2)*100/profit
        )
        
      
    
    
        
    
    
        
    
    